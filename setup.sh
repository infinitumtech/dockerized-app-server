#!/usr/bin/env bash

if [ ! -f .env ]; then
  echo "Error! .env file does not exists, create first from env.example"
  exit 1
fi

. .env

echo "creating redis data ..."
mkdir -p volumes/redis/data

echo "creating database volumes ..."
mkdir -p volumes/mariadb/data
mkdir -p volumes/postgres/data

echo "creating app volumes ..."
mkdir -p volumes/apps/${NGINX_HOSTNAME}

echo "Preparing traefik certificates resolver stores ..."
touch volumes/traefik/acme.json
chmod 600 volumes/traefik/acme.json
touch volumes/traefik/acme-cloudflare.json
chmod 600 volumes/traefik/acme-cloudflare.json

