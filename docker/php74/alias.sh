# Add aliases for xdebug control
alias xoff="mv /usr/local/etc/php/conf.d/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini.off && kill -USR2 1"
alias xon="mv /usr/local/etc/php/conf.d/xdebug.ini.off /usr/local/etc/php/conf.d/xdebug.ini && kill -USR2 1"

# Add aliases for opcache control
alias opon="sed 's|opcache.enable=0|opcache.enable=1|' /usr/local/etc/php/conf.d/zz-local.ini > /tmp/zz-local.ini && cat /tmp/zz-local.ini > /usr/local/etc/php/conf.d/zz-local.ini && kill -USR2 1"
alias opoff="sed 's|opcache.enable=1|opcache.enable=0|' /usr/local/etc/php/conf.d/zz-local.ini > /tmp/zz-local.ini && cat /tmp/zz-local.ini > /usr/local/etc/php/conf.d/zz-local.ini && kill -USR2 1"

# additional aliases
alias ls="ls --color=tty"
alias ll="ls -lh"
