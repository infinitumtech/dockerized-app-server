# reference: https://github.com/prooph/docker-files/blob/master/php/7.4-fpm

FROM php:8.1-fpm-alpine

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && composer --version

# Set timezone
RUN apk add --no-cache tzdata \
    && ln -s -f /usr/share/zoneinfo/Asia/Kuala_Lumpur /etc/localtime \
    && "date"

# persistent / runtime deps
ENV PHPIZE_DEPS \
    autoconf \
    cmake \
    file \
    g++ \
    gcc \
    libc-dev \
    pcre-dev \
    make \
    git \
    pkgconf \
    re2c \
    freetype-dev \
    libpng-dev  \
    libjpeg-turbo-dev \
    libxslt-dev \
    icu-dev \
    openssl-dev \
    curl-dev \
    imagemagick-dev \
    libtool

RUN apk add --no-cache --virtual .persistent-deps \
    icu-libs \
    libssl1.1 \
    postgresql-dev \
    libxml2-dev \
    # libressl-dev \
    freetype \
    libpng \
    libjpeg-turbo \
    libxslt \
    oniguruma-dev \
    libgcrypt \
    zlib-dev \
    libzip-dev \
    curl \
    imagemagick \
    imagemagick-libs \
    libgomp \
    mysql-client \
    openssh-client \
    postgresql-libs \
    rsync \
    vim \
    supervisor \
    sudo

RUN set -xe \
    # workaround for rabbitmq linking issue
    && ln -s /usr/lib /usr/local/lib64 \
    # hack to link libgcrypt
    && ln -s /usr/lib/libgcrypt.so.20 /usr/lib/libgcrypt.so \
    && ln -s /usr/lib/libgpg-error.so.0 /usr/lib/libgpg-error.so \
    && apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
    && pecl install imagick && docker-php-ext-enable imagick \
    # 7.4 changes https://github.com/php/php-src/blob/PHP-7.4/UPGRADING
    && docker-php-ext-configure gd \
        --enable-gd \
        --with-freetype=/usr/include/ \
        --with-jpeg=/usr/include/ \
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure mysqli --with-mysqli \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-configure pdo_pgsql --with-pdo-pgsql \
    && docker-php-ext-configure mbstring --enable-mbstring \
    && docker-php-ext-configure soap --enable-soap \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        calendar \
        curl \
        exif \
        gd \
        # iconv \
        intl \
        mbstring \
        mysqli \
        opcache \
        pcntl \
        pdo \
        pdo_mysql \
        pdo_pgsql \
        soap \
        # tokenizer \
        xml \
        xsl \
        zip \
    && docker-php-ext-enable pdo_mysql

# Possible values for ext-name:
# bcmath bz2 calendar ctype curl dba dom enchant exif fileinfo filter ftp gd gettext gmp hash iconv imap interbase intl
# json ldap mbstring mcrypt mssql mysql mysqli oci8 odbc opcache pcntl pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci
# pdo_odbc pdo_pgsql pdo_sqlite pgsql phar posix pspell readline recode reflection session shmop simplexml snmp soap
# sockets spl standard sybase_ct sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader xmlrpc xmlwriter xsl zip

# PHPRedis
ENV PHP_REDIS_VERSION 5.3.7
RUN git clone --branch ${PHP_REDIS_VERSION} https://github.com/phpredis/phpredis /tmp/phpredis \
        && cd /tmp/phpredis \
        && phpize  \
        && ./configure  \
        && make  \
        && make install \
        && make test

# Copy modules/extension configuration
COPY docker/php81/config/opcache.ini $PHP_INI_DIR/conf.d/
COPY docker/php81/config/redis.ini $PHP_INI_DIR/conf.d/

# Copy custom configuration
COPY docker/php81/config/php7.ini $PHP_INI_DIR/conf.d/
COPY docker/php81/config/fpm/php-fpm.conf /usr/local/etc/
COPY docker/php81/config/fpm/pool.d /usr/local/etc/pool.d
COPY docker/php81/local.ini $PHP_INI_DIR/conf.d/zz-local.ini

# cleanup
RUN apk del .build-deps \
    && rm -rf /tmp/* \
    && rm -rf /app \
    && mkdir /app

# Add local aliases for xdebug, opcache controls
COPY docker/php81/alias.sh /etc/profile.d/alias.sh

# Add user/group for laravel application
RUN addgroup -g 1000 www \
    && adduser -u 1000 -D -s /bin/ash -G www www

# cronjob for user
COPY docker/php81/crontab /crontab
RUN /usr/bin/crontab -u www /crontab

# supervisor queue
RUN chmod 644 /etc/supervisord.conf
RUN mkdir -p /etc/supervisor.d
COPY docker/php81/laravel-worker.ini /etc/supervisor.d/laravel-worker.ini
RUN mkdir -p /var/www/html/storage/logs

# setup sudo
RUN echo '%wheel ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/wheel
RUN adduser www wheel

USER www

VOLUME ["/var/www"]
WORKDIR /var/www
