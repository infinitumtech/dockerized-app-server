### Nginx config

#### Example:
```
server {
    server_name kehati.my www.kehati.my;
    listen 80;

    root /var/www/html/kehati.my;

    # Security headers
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-XSS-Protection "1; mode=block" always;
    add_header X-Content-Type-Options "nosniff" always;
    add_header Referrer-Policy "no-referrer-when-downgrade" always;
    add_header Content-Security-Policy "default-src 'self' http: https: data: blob: 'unsafe-inline'" always;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;

    # dot files
    location ~ /\.(?!well-known) { deny all; }

    # SEO files
    location = /robots.txt { log_not_found off; }
    location = /sitemap.xml { log_not_found off; }
    location = /favicon.ico { log_not_found off; }

    charset utf-8;
    client_max_body_size 128M;
    index index.php index.html;

    access_log /var/log/nginx/kehati.my-access.log;
    # access_log off;
    error_log  /var/log/nginx/kehati.my-error.log;

    # gzip compression settings
    gzip on;
    gzip_comp_level 5;
    gzip_min_length 256;
    gzip_proxied any;
    gzip_vary on;

    ## browser cache control
    #location ~* \.(ico|css|js|gif|jpeg|jpg|png|woff|ttf|otf|svg|woff2|eot)$ {
    #    expires 1d;
    #    access_log off;
    #    add_header Pragma public;
    #    add_header Cache-Control "public, max-age=86400";
    #}

    location / {
        try_files $uri $uri/ /index.php?$query_string;
        gzip_static on;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass app-php-74:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location ~ /\.ht {
        deny all;
    }
}
```
