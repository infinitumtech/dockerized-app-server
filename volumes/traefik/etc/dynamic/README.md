### Dynamic Traefik file provider

#### Example:
```yaml
http:
  routers:
    kedaikita-com:
      rule: "Host(`kedaikita.com`) || Host(`www.kedaikita.com`)"
      service: "kedaikita-com"
      tls:
        certResolver: cloudflare
    kehati-my:
      rule: "Host(`kehati.my`) || Host(`www.kehati.my`)"
      service: "kehati-my"
      tls:
        certResolver: cloudflare

  services:
    kedaikita-com:
      loadBalancer:
        servers:
          - url: "http://178.20.0.80:80"
        passHostHeader: true
    kehati-my:
      loadBalancer:
        servers:
          - url: "http://178.20.0.80:80"
        passHostHeader: true
```
